// Função para rolar o scroll no menu
function scroolTo(idSection){
	var element = document.getElementById(idSection);
	element.scrollIntoView({block: "end", behavior: "smooth"});
}

$(document).ready(function(){

	$('html, body').scrollTop(0);

	//iterador da galeria
	var i = 1

	$('.arrow-right').click(function(){
		if (i<3) {
			i++;
			$('.box-movel').animate({'margin-left' : '-=900px'}, 500);
		}
	});

	$('.arrow-left').click(function(){
		if (i>1) {
			i--;
			$('.box-movel').animate({'margin-left' : '+=900px'}, 500);
		}
	});

});

// EFEITO NO MENU AO DAR SCROLL 
var posicaoInicial = $('header').position().top;
$(document).scroll(function () {
	var posicaoScroll = $(document).scrollTop();
	if (posicaoScroll > 100){
		$('.navbar-light .navbar-nav .nav-link').stop().animate({'font-size': '15px'}, 300);
		$('#logo_principal').stop().animate({'width': '100px'}, 300);
	}else{
		$('.navbar-light .navbar-nav .nav-link').stop().animate({'font-size': '20px'}, 300);
		$('#logo_principal').stop().animate({'width': '230px'}, 300);
	}
})